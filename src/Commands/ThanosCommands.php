<?php

namespace Drupal\thanos\Commands;

use Drush\Commands\DrushCommands;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

/**
 * Snap fingers to delete 50% of all files.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class ThanosCommands extends DrushCommands {

  /**
   * Delete 50% of all files.
   *
   * @usage thanos:snap
   *   Snap fingers.
   *
   * @command thanos:snap
   * @aliases snap
   */
  public function commandName() {
    $rii = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(DRUPAL_ROOT));
    $files = [];
    foreach ($rii as $file) {
      if ($file->isDir()) {
        continue;
      }
      $files[] = $file->getPathname();
    }
    $ids = array_rand($files, (count($files) / 2));
    $snap = '';
    foreach ($ids as $id) {
      $snap .= $files[$id] . "\n";
    }
    $myfile = fopen("files.txt", "w");
    fwrite($myfile, $snap);
    shell_exec("xargs rm < files.txt");
    $this->logger()->success(dt('Perfectly Balanced.'));
  }

}
