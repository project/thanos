CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Conflicts/Known issues


INTRODUCTION
------------

Thanos module provides a Drush command that deletes 50% of your files.
Random, dispassionate.


REQUIREMENTS
------------

The hardest choices require the hardest wills.

CONFLICTS/KNOWN ISSUES
----------------------

Once you snap, your universe will not be the same as it was before.
